import React from 'react';
import { render } from '@testing-library/react';
import { it, expect } from '@jest/globals';
import { Column } from '../components';

it('Компонент Column должен принимать пропс name и отображать его', () => {
  const columnName = 'TODO';

  const { container } = render(<Column name={columnName} />);

  expect(container).toHaveTextContent(columnName);
});
